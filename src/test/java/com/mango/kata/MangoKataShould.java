package com.mango.kata;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import com.mango.kata.MangoKata;

/**
 * @author lperez
 */
public class MangoKataShould {

    @Test
    public void print_man_when_multiple_of_three() {
        MangoKata mangoKata = new MangoKata();
        assertThat(mangoKata.transform(9), is("Man"));
    }

    @Test
    public void print_go_when_multiple_of_five() {
        MangoKata mangoKata = new MangoKata();
        assertThat(mangoKata.transform(25), is("go"));
    }

    @Test
    public void print_mango_when_multiple_of_three_and_five() {
        MangoKata mangoKata = new MangoKata();
        assertThat(mangoKata.transform(45), is("Mango"));
    }

}
